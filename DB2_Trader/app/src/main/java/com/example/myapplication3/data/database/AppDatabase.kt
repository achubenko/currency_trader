package com.example.myapplication3.data.database

import com.example.myapplication3.data.repository.NbuCurrencyRepository

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.myapplication3.data.models.PbCurrency


@Database(version = 9, exportSchema = false, entities = [PbCurrency::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieRequestDao(): CurrencyRatesDao

    companion object {

        private val TAG = AppDatabase::class.java.simpleName

        internal val NAME = "app_database.db"

        private var mAppDatabase: AppDatabase? = null
        private val M_NBU_CURRENCY_REPOSITORY: NbuCurrencyRepository? = null

        @Synchronized
        fun build(context: Context): AppDatabase {
            Log.d(TAG, "build()")
            if (mAppDatabase == null) {
                mAppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.NAME)
                        .addCallback(object : RoomDatabase.Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {

                            }

                            override fun onOpen(db: SupportSQLiteDatabase) {

                            }
                        }).fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
            }
            return mAppDatabase as AppDatabase
        }
    }

}
