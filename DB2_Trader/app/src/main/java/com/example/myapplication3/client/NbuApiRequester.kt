package com.example.myapplication3.client

import com.example.myapplication3.data.models.NbuSingleCurrency
import com.example.myapplication3.data.models.PbNetworkResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NbuApiRequester {

    @GET("NBUStatService/v1/statdirectory/exchange")
    fun getNbuRateForCurrencyAndDateFromNetwork(
            @Query("valcode") valcode: String, @Query("date") date: String,
            @Query("json") json: String = ""): Observable<NbuSingleCurrency>

}
