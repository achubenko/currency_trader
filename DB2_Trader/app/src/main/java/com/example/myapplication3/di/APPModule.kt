package com.example.myapplication3.di


import android.app.Application
import android.util.Log
import com.example.myapplication3.client.NbuApiRequester

import com.example.myapplication3.client.PbApiRequester
import com.example.myapplication3.data.database.AppDatabase
import com.example.myapplication3.data.database.CurrencyRatesDao

import com.google.gson.Gson

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module(includes = [ViewModelModule::class])
class APPModule {

    companion object {
        private val TAG = APPModule::class.java.simpleName
        private val PB_URL = "https://api.privatbank.ua/"
        private val NBU_URL = "https://bank.gov.ua/"
    }

    @Provides
    @Singleton
    fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
        Log.d(TAG, "getHttpLoggingInterceptor()")
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    fun getPbApiRequester(@Named("pb") retrofit: Retrofit): PbApiRequester {
        Log.d(TAG, "getPbApiRequester()")
        return retrofit.create(PbApiRequester::class.java)
    }

    @Provides
    @Singleton
    fun getNbuApiRequester(@Named("nbu") retrofit: Retrofit): NbuApiRequester {
        Log.d(TAG, "getPbApiRequester()")
        return retrofit.create(NbuApiRequester::class.java)
    }

    @Provides
    @Singleton
    @Named("pb")
    fun getPbRetrofit(okHttpClient: OkHttpClient): Retrofit {
        Log.d(TAG, "getPbRetrofit()")
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(PB_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()
    }

    @Provides
    @Singleton
    @Named("nbu")
    fun getNbuRetrofit(okHttpClient: OkHttpClient): Retrofit {
        Log.d(TAG, "getPbRetrofit()")
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(NBU_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()
    }

    @Provides
    @Singleton
    fun getOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        Log.d(TAG, "getOkHttpClient()")
        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor).build()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(application: Application) = AppDatabase.build(application)

    @Provides
    @Singleton
    fun provideMovieRequestDao(appDatabase: AppDatabase): CurrencyRatesDao = appDatabase.movieRequestDao()

}
