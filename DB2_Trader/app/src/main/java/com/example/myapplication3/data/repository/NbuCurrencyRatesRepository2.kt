package com.example.myapplication3.data.repository

import javax.inject.Inject
import javax.inject.Singleton

import androidx.lifecycle.MutableLiveData
import com.example.myapplication3.client.NbuApiRequester
import com.example.myapplication3.data.models.NbuSingleCurrency
import com.example.myapplication3.util.Converters.convertNbuCalToStringDate
import io.reactivex.Observable
import java.util.*

import kotlin.collections.ArrayList

@Singleton
open class NbuCurrencyRatesRepository2 @Inject
constructor(private val nbuApi: NbuApiRequester) {

    companion object {
        private val TAG = NbuCurrencyRatesRepository2::class.java.simpleName
    }

    val nbuSingleCurrencyRatesMutableLiveData: MutableLiveData<List<NbuSingleCurrency>> = MutableLiveData()

    fun loadSingleCurrencyRatesForPeriod(curLib: String, period: Int = 30){
        val curRequestList: List<Observable<NbuSingleCurrency>> = getcurRequestListForPeriod(curLib, period)
        val nbuRatesList: MutableList<NbuSingleCurrency> = ArrayList(period)
        //fixme
//        Observable.zip(curRequestList){t->t}.doOnNext({nbuRatesList.add(it)})
//                .subscribe({nbuSingleCurrencyRatesMutableLiveData.value= nbuRatesList})
    }

    fun getcurRequestListForPeriod(curLib: String, period: Int): List<Observable<NbuSingleCurrency>>{
        var curRequestList: MutableList<Observable<NbuSingleCurrency>> = ArrayList(period)
        var cal = Calendar.getInstance()
        for(n in 0..29){
            cal.roll(Calendar.DATE, -1)
            val nbuRequestDate = convertNbuCalToStringDate(cal)
            curRequestList.add(nbuApi.getNbuRateForCurrencyAndDateFromNetwork(curLib, nbuRequestDate))
        }
        return curRequestList
    }
}
