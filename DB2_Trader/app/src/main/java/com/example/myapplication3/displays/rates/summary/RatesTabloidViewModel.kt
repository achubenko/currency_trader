package com.example.myapplication3.displays.rates.summary


import com.example.myapplication3.data.repository.NbuCurrencyRepository
import android.util.Log
import javax.inject.Inject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication3.data.models.PbCurrency

class RatesTabloidViewModel @Inject
constructor(private val repository: NbuCurrencyRepository) : ViewModel() {

    private var movieTempName: String? = null

    val pbRatesMutableLiveData: MutableLiveData<List<PbCurrency>>
    val errorMutableLiveData: MutableLiveData<Error>

    init {
        Log.d(TAG, "constructor")
        this.pbRatesMutableLiveData = repository.pbRatesMutableLiveData
        errorMutableLiveData = MutableLiveData()
    }

    fun loadActualRatesFromRepository(){
        repository.loadAllActualRates()
    }

    companion object {

        private val TAG = RatesTabloidViewModel::class.java.simpleName
    }
}
