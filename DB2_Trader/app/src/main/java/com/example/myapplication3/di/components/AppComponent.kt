package com.example.myapplication3.di.components

import com.example.myapplication3.MovieApp
import com.example.myapplication3.di.APPModule
import com.example.myapplication3.di.builder.ActivityBuilderModule

import android.app.Application

import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

@Singleton
@Component(modules = [APPModule::class, AndroidInjectionModule::class, ActivityBuilderModule::class])
interface AppComponent : AndroidInjector<MovieApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(movieApp: MovieApp)
}