package com.example.myapplication3.util

import android.util.Log
import java.lang.StringBuilder
import java.util.*

object Converters {

    fun convertPbStringDateToLong(dateString: String?):Long {
        dateString.let {
        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_MONTH, dateString!!.substring(0, 1).toInt())
        cal.set(Calendar.MONTH, (dateString.substring(3, 4).toInt()-1))
        cal.set(Calendar.YEAR, dateString.substring(6, 9).toInt())
        return cal.timeInMillis }
        return 0L
    }

    fun convertPbLongToStringDate(dateLong: Long):String{
        val cal = Calendar.getInstance()
        cal.time=Date(dateLong)
        return StringBuilder().append(cal.get(Calendar.MONTH)).append(".")
                .append(cal.get(Calendar.MONTH)+1).append(".").append(cal.get(Calendar.YEAR)).toString()
    }

    fun convertNbuStringDateToLong(dateString: String?):Long {

        dateString.let {
            val cal = Calendar.getInstance()
            cal.set(Calendar.DAY_OF_YEAR, dateString!!.substring(6, 9).toInt())
            cal.set(Calendar.MONTH, (dateString.substring(3, 4).toInt()-1))
            cal.set(Calendar.DAY_OF_MONTH, dateString!!.substring(0, 1).toInt())
            return cal.timeInMillis }
    }

    fun convertNbuStringDateToDate(dateString: String?):Date {
        Log.d("convertNbuStringDateToDate()", dateString)
        dateString.let {
            val cal = Calendar.getInstance()
            cal.set(Calendar.DAY_OF_YEAR, dateString!!.substring(6, 9).toInt())
            cal.set(Calendar.MONTH, (dateString.substring(3, 4).toInt()-1))
            cal.set(Calendar.DAY_OF_MONTH, dateString!!.substring(0, 1).toInt())
            return cal.time }
    }

    fun convertNbuLongToStringDate(dateLong: Long):String{
        val cal = Calendar.getInstance()
        cal.time=Date(dateLong)
        return StringBuilder().append(cal.get(Calendar.YEAR)).append(cal.get(Calendar.MONTH)+1)
                .append(cal.get(Calendar.DAY_OF_MONTH)).toString()
    }

    fun convertNbuCalToStringDate(cal: Calendar):String{
        return StringBuilder().append(cal.get(Calendar.YEAR)).append(cal.get(Calendar.MONTH)+1)
                .append(cal.get(Calendar.DAY_OF_MONTH)).toString()
    }
}
