package com.example.myapplication3


import android.app.Activity
import android.app.Application

import com.example.myapplication3.di.components.DaggerAppComponent

import javax.inject.Inject

import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector

class MovieApp : Application(), HasActivityInjector {

    @set:Inject
    internal var activityDispatchingInjector: DispatchingAndroidInjector<Activity>? = null

    override fun onCreate() {
        super.onCreate()
        initializeComponent()
        setInstance(this)
    }

    private fun initializeComponent() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return activityDispatchingInjector
    }

    companion object {

        var appContext: MovieApp? = null
            private set


        @Synchronized
        private fun setInstance(app: MovieApp) {
            appContext = app
        }
    }
}
