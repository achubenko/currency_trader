package com.example.myapplication3.displays.rates.summary

import com.example.myapplication3.R
import com.example.myapplication3.util.EspressoIdlingResource

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import javax.inject.Inject
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication3.data.models.PbCurrency
import dagger.android.support.AndroidSupportInjection

class RatesTabloidFragment : Fragment(){

    private lateinit var nbuRatesRecyclerView: RecyclerView
    private lateinit var mRatesTabloidViewModel: RatesTabloidViewModel
    private lateinit var moviesAdapter: RatesListRecyclerAdapter

    private lateinit var dateTV1: TextView
    private lateinit var dateTV2: TextView

    @set:Inject
    var viewModelFactory: ViewModelProvider.Factory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        EspressoIdlingResource.increment()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.fragment_currency_rates, container, false)
        tuneMoviesGridRecyclerView(inflatedView)
        setHasOptionsMenu(true)
        startObservation()
        return inflatedView
    }

    private fun initDateTv(inflatedView: View){
        dateTV1 = inflatedView.findViewById(R.id.date_pb_text_view1)
        dateTV2 = inflatedView.findViewById(R.id.date_pb_text_view2)

//        dateTV1.setOnClickListener { object View.OnClickListenr()}

    }

    private fun startObservation() {
        mRatesTabloidViewModel = ViewModelProviders.of(this, viewModelFactory).get(RatesTabloidViewModel::class.java)
        mRatesTabloidViewModel.pbRatesMutableLiveData
                .observe(this, Observer<List<PbCurrency>> { this.renewRecycleView(it) })
        mRatesTabloidViewModel.errorMutableLiveData.observe(this, Observer<Error> { this.actOnFailure(it) })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRatesTabloidViewModel.loadActualRatesFromRepository()
    }


    fun renewRecycleView(rates: List<PbCurrency>) {
        moviesAdapter!!.setMoviePreviewList(rates)
        moviesAdapter!!.notifyDataSetChanged()
    }

    fun actOnFailure(error: Error) {
        Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
    }

    private fun tuneMoviesGridRecyclerView(inflatedView: View) {
        nbuRatesRecyclerView = inflatedView.findViewById(R.id.nbu_rates_recycler_view)
        nbuRatesRecyclerView.layoutManager = LinearLayoutManager(context)
        nbuRatesRecyclerView.itemAnimator = DefaultItemAnimator()
        moviesAdapter = RatesListRecyclerAdapter(this)
        nbuRatesRecyclerView.adapter = moviesAdapter
    }

    fun dateDialog(){

    }

    override fun onResume() {
        super.onResume()
        mRatesTabloidViewModel!!.loadActualRatesFromRepository()
        EspressoIdlingResource.decrement()
    }

    companion object {

        private val QUERY_KEY = "movieTextQuery"
        private val TAG = RatesTabloidFragment::class.java.simpleName
    }
}
