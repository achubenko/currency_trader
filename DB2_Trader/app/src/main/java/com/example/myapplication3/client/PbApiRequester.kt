package com.example.myapplication3.client

import com.example.myapplication3.data.models.PbNetworkResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PbApiRequester {

    @GET("p24api/exchange_rates")
    fun getRateForDateFromNetwork(@Query("json") json: String = "", @Query("date") date: String): Call<PbNetworkResponse>

}
