package com.example.myapplication3.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NbuSingleCurrency {

    @SerializedName("currencyCode")
    @Expose
    var currencyCode: Int? = null
    @SerializedName("currencyNameUa")
    @Expose
    var currencyNameUa: String? = null
    @SerializedName("rateToHrn")
    @Expose
    var rateToHrn: Float? = null
    @SerializedName("currencyNameLit")
    @Expose
    var currencyNameLit: String? = null
    @SerializedName("exchangeDate")
    @Expose
    var exchangeDate: String? = null

    constructor(r030: Int?, txt: String?, rate: Float?, cc: String?, exchangedate: String?){
        this.currencyCode = r030
        this.currencyNameUa = txt
        this.rateToHrn = rate
        this.currencyNameLit = cc
        this.exchangeDate = exchangedate
    }
}