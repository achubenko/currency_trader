package com.example.myapplication3.displays.rates

import com.example.myapplication3.R
import com.example.myapplication3.displays.rates.summary.RatesTabloidFragment

import android.os.Bundle
import android.util.Log

import javax.inject.Inject

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @set:Inject
    internal var fragmentAndroidInjector: DispatchingAndroidInjector<Fragment>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate invoked")
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val manager = supportFragmentManager
        if (savedInstanceState == null) {
            val swiperFragment = RatesTabloidFragment()
            manager.beginTransaction()
                    .replace(R.id.fragment_container, swiperFragment)
                    .commit()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentAndroidInjector
    }

    companion object {

        private val TAG = MainActivity::class.java.simpleName
    }
}
