package com.example.myapplication3.displays.rates.summary

interface MovieRequestItemGetter {
    fun getMovieNamesFormDB(movieName: String)
}
