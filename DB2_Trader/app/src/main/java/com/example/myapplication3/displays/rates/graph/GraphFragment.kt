package com.example.myapplication3.displays.rates.graph

import com.example.myapplication3.R


import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import javax.inject.Inject
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication3.data.models.NbuSingleCurrency
import com.example.myapplication3.util.Converters.convertNbuStringDateToDate
import com.example.myapplication3.util.Converters.convertNbuStringDateToLong
import dagger.android.support.AndroidSupportInjection

import com.example.myapplication3.util.ParsingConstants.Companion.APP_SETS
import com.example.myapplication3.util.ParsingConstants.Companion.CURRENCY_LIT
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

class GraphFragment : Fragment() {

    @set:Inject
    var viewModelFactory: ViewModelProvider.Factory? = null

    private var curLit: String? = null
    private var graphViewModel: GraphViewModel? = null

    private var graph: GraphView? = null

    private var actionBar: ActionBar? = null
    private var permissionFlag: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            curLit = arguments!!.getString(CURRENCY_LIT)
            Log.d(TAG, "curLit: " + curLit!!)
        } else if (savedInstanceState != null) {
            curLit = savedInstanceState.getString(CURRENCY_LIT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.fragment_currency_graph, container, false)
        graph = inflatedView.findViewById(R.id.currency_graph) as GraphView
        return inflatedView
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        startObservation()
        setHasOptionsMenu(true)
        actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        graphViewModel!!.loadRatesForPeriod(curLit!!)
    }

    private fun startObservation() {
        graphViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(GraphViewModel::class.java)
        //FIXME remove binding from viewmodel
        graphViewModel!!.initViewModelFields()
        graphViewModel!!.nbuCurrencyMutableLiveData!!.observe(this, Observer<List<NbuSingleCurrency>> { this.drawGraph(it) })
        graphViewModel!!.errorMutableLiveData.observe(this, Observer<Error> { this.actOnFailure(it) })
        graphViewModel!!.toastMessageMutableLiveData!!.observe(this, Observer<String> { this.showToast(it) })
    }

    private fun drawGraph(ratesList: List<NbuSingleCurrency>){
        Log.d(TAG, "drawGraph()")
        var series: LineGraphSeries<DataPoint> = LineGraphSeries<DataPoint>()
        for (r in ratesList.reversed()) {
            series.appendData(DataPoint(convertNbuStringDateToDate(r.exchangeDate), r.rateToHrn!!.toDouble()), true, Math.round(r.rateToHrn!!), true)
        }
        graph!!.addSeries(series)
    }


    private fun actOnFailure(error: Error) {
        showToast(error.message!!)
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        graphViewModel!!.onOptionsItemSelected(item.itemId, !item.isChecked)
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() invoked")
        //TODO it should be done later
        //        if (task!=null&&task.getStatus()== AsyncTask.Status.RUNNING){
        //            getArguments().putSerializable(TASK, task);
        //        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState(...) invoked")
        outState.putString(CURRENCY_LIT, curLit)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WRITE_EXTERNAL_STORAGE_CODE -> {
                permissionFlag = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                return
            }
        }
    }

    companion object {

        val TASK = "task"

        val WRITE_EXTERNAL_STORAGE_CODE = 112
        private val TAG = GraphFragment::class.java.simpleName
    }
}
