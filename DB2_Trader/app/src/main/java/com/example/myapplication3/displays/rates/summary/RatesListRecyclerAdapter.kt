package com.example.myapplication3.displays.rates.summary

import android.annotation.SuppressLint
import com.example.myapplication3.R
import com.example.myapplication3.displays.rates.graph.GraphFragment
import com.example.myapplication3.util.OnClickListener

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.util.ArrayList
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication3.data.models.PbCurrency
import com.example.myapplication3.databinding.CardCurrencyRateBinding

import com.example.myapplication3.util.ParsingConstants.Companion.CURRENCY_LIT

class RatesListRecyclerAdapter(private val fragment: Fragment) : RecyclerView.Adapter<RatesListRecyclerAdapter.SearchedMovieViewHolder2>(), OnClickListener {
    private val mCurrencyList = ArrayList<PbCurrency>()

    fun setMoviePreviewList(currencyList: List<PbCurrency>) {
        this.mCurrencyList.clear()
        this.mCurrencyList.addAll(currencyList)
        //        if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
        //            EspressoIdlingResource.decrement(); // Set app as idle.
        //        }
//        EspressoIdlingResource.decrement()
    }

    override fun openDetailedFragment(bundle: Bundle, backStackTag: String) {
        val detailFragment = GraphFragment()
        detailFragment.arguments = bundle
        fragment.activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, detailFragment)
                .addToBackStack(backStackTag).commit()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SearchedMovieViewHolder2 {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val binding = CardCurrencyRateBinding.inflate(layoutInflater, viewGroup, false)
        return SearchedMovieViewHolder2(binding.root, this)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(viewHolder: SearchedMovieViewHolder2, i: Int) {
        val curRate: PbCurrency = mCurrencyList[i]
        viewHolder.binding!!.curRate = curRate
        if (i % 2 ==0 ) {
            viewHolder.binding!!.currencyCard.setBackgroundColor(R.color.lightGrey)
        }
        viewHolder.setCurLit(curRate.currency)
    }

    override fun getItemCount(): Int {
        return mCurrencyList.size
    }

    class SearchedMovieViewHolder2(v: View, private val onClickListener: OnClickListener) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var curLit: String? = null

        var binding: CardCurrencyRateBinding? = null

        fun setCurLit(curLit: String) {
            this.curLit = curLit
        }

        init {
            binding = DataBindingUtil.bind(v)
            itemView.setOnClickListener(this)
        }

        override fun onClick(itemView: View) {
            val bundle = Bundle()
            bundle.putString(CURRENCY_LIT, curLit)
            onClickListener.openDetailedFragment(bundle, TAG)
        }

        companion object {

            private val TAG = SearchedMovieViewHolder2::class.java.simpleName
        }
    }

    companion object {

        private val TAG = RatesListRecyclerAdapter::class.java.simpleName

    }
}
