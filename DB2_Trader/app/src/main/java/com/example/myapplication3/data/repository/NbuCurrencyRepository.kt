package com.example.myapplication3.data.repository

import android.util.Log
import com.example.myapplication3.data.database.CurrencyRatesDao

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import javax.inject.Inject
import javax.inject.Singleton

import androidx.lifecycle.MutableLiveData
import com.example.myapplication3.client.NbuApiRequester
import com.example.myapplication3.client.PbApiRequester
import com.example.myapplication3.data.models.NbuSingleCurrency
import com.example.myapplication3.data.models.PbCurrency
import com.example.myapplication3.data.models.PbNetworkResponse
import com.example.myapplication3.util.Converters.convertNbuCalToStringDate
import com.example.myapplication3.util.Converters.convertPbLongToStringDate
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

import kotlin.collections.ArrayList

@Singleton
open class NbuCurrencyRepository @Inject
constructor(private val mCurrencyRatesDao: CurrencyRatesDao, private val pbApi: PbApiRequester, private val nbuApi: NbuApiRequester) {

    companion object {
        private val TAG = NbuCurrencyRepository::class.java.simpleName
    }

    private val executor: ExecutorService  = Executors.newSingleThreadScheduledExecutor()
    val pbRatesMutableLiveData: MutableLiveData<List<PbCurrency>> = MutableLiveData()
    val nbuSingleCurrencyRatesMutableLiveData: MutableLiveData<List<NbuSingleCurrency>> = MutableLiveData()
    val errorMutableLiveData: MutableLiveData<Error> = MutableLiveData()

    fun loadAllActualRates() {
        Log.d(TAG, "loadAllActualRates() invoked")
        val dateLong = System.currentTimeMillis()
        var currenciesRates =  mCurrencyRatesDao.getAllCurrenciesForDateFromDb(dateLong)
        if(currenciesRates.isNotEmpty()){ pbRatesMutableLiveData.value = currenciesRates}
        else{
            loadCurrenciesForDateFromPb(convertPbLongToStringDate(dateLong))
        }
    }

    fun loadCurrenciesForDateFromPb(date: String){
        Log.d(TAG, "loadCurrenciesForDateFromPb invoked")
        pbApi.getRateForDateFromNetwork(date = date)
                .enqueue(object : Callback<PbNetworkResponse>{
                    override fun onResponse(call: Call<PbNetworkResponse>, response: Response<PbNetworkResponse>) {
                        pbRatesMutableLiveData.value = response.body()?.getStoredCurrencyModelList()
                    }

                    override fun onFailure(call: Call<PbNetworkResponse>, t: Throwable) {
                        val error = Error("Error Fetching Data!")
                        errorMutableLiveData.value = error
                    }
                }
        )
    }

    fun loadSingleCurrencyRatesForPeriod(curLib: String, period: Int = 30){
        val curRequestList: List<Observable<NbuSingleCurrency>> = getcurRequestListForPeriod(curLib, period)
        val nbuRatesList: MutableList<NbuSingleCurrency> = ArrayList(period)
        Observable.merge(curRequestList).doOnNext{nbuRatesList.add(it)}.subscribe({nbuSingleCurrencyRatesMutableLiveData.value= nbuRatesList},{}).dispose()
        nbuSingleCurrencyRatesMutableLiveData.value = getMockedNbuCurrencyList()
    }

    fun getMockedNbuCurrencyList():List<NbuSingleCurrency> {
        val nbuRatesList: MutableList<NbuSingleCurrency> = ArrayList(30)
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 28.988385F, "EUR", "15.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.117373F, "EUR", "14.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.117373F, "EUR", "13.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.117373F, "EUR", "12.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро",	28.835932F, "EUR", "11.07.2019"))

        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 28.560624F, "EUR", "10.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 28.712824F, "EUR", "09.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 28.950992F, "EUR", "08.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.202999F, "EUR", "07.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.202999F, "EUR", "06.07.2019"))

        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.202999F, "EUR", "05.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.412439F, "EUR", "04.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.584925F, "EUR", "03.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.741749F, "EUR", "02.07.2019"))
        nbuRatesList.add(NbuSingleCurrency(	978, "Євро", 29.764317F, "EUR", "01.07.2019"))

        return nbuRatesList
    }

    fun getcurRequestListForPeriod(curLib: String, period: Int): List<Observable<NbuSingleCurrency>>{
        var curRequestList: MutableList<Observable<NbuSingleCurrency>> = ArrayList(period)
        var cal = Calendar.getInstance()
        for(n in 0..29){
            cal.roll(Calendar.DATE, -1)
            val nbuRequestDate = convertNbuCalToStringDate(cal)
            curRequestList.add(nbuApi.getNbuRateForCurrencyAndDateFromNetwork(curLib, nbuRequestDate))
        }
        return curRequestList
    }

//    fun delete(movieRequest: MovieRequest) {
//        mCurrencyRatesDao.delete(movieRequest)
//    }
}
