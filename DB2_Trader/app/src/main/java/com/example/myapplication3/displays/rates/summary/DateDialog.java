package com.example.myapplication3.displays.rates.summary;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    private Bundle transferBundle;

    private int yearIntValue;
    private int monthIntValue;
    private int dayIntValue;

    private static final String YEAR = "yearIntValue";
    private static final String MONTH = "monthIntValue";
    private static final String DAY = "dayIntValue";


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        transferBundle = getArguments();
        Calendar cal = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(),this,
                cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        return dialog;
    }


    private void restoreFieldsValues(@Nullable Bundle savedInstanceState){
        if(savedInstanceState==null){
            Calendar calendar = Calendar.getInstance();
            yearIntValue = calendar.get(Calendar.YEAR);
            monthIntValue = calendar.get(Calendar.MONTH);
            dayIntValue = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            yearIntValue = savedInstanceState.getInt("yearIntValue");
            monthIntValue = savedInstanceState.getInt("monthIntValue");
            dayIntValue = savedInstanceState.getInt("dayIntValue");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("yearIntValue", yearIntValue);
        outState.putInt("monthIntValue", monthIntValue);
        outState.putInt("dayIntValue", dayIntValue);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        transferBundle.putInt(YEAR, year);
        transferBundle.putInt(MONTH, month);
        transferBundle.putInt(DAY, dayOfMonth);
//        TextView showerTextView = getActivity().findViewById(R.id.);
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(year, month, dayOfMonth);
//        String dateString = DateFormat.getDateInstance().format(calendar.getTime());
//        showerTextView.setText(dateString);
    }
}

