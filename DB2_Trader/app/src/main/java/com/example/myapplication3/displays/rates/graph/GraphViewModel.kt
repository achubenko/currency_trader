package com.example.myapplication3.displays.rates.graph

import com.example.myapplication3.R
import com.example.myapplication3.displays.rates.summary.RatesTabloidViewModel

import android.content.SharedPreferences

import javax.inject.Inject

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication3.data.models.NbuSingleCurrency
import com.example.myapplication3.data.models.PbCurrency
import com.example.myapplication3.data.repository.NbuCurrencyRepository

import com.example.myapplication3.util.ParsingConstants.Companion.OFFLINE


class GraphViewModel @Inject
constructor(private val repository: NbuCurrencyRepository) : ViewModel() {

    private var mDetailsView: GraphRoomContract.DetailsView? = null
    private var sharedPreferences: SharedPreferences? = null
    var pbCurrencyMutableLiveData: MutableLiveData<List<PbCurrency>>? = null
    var nbuCurrencyMutableLiveData: MutableLiveData<List<NbuSingleCurrency>>? = null
        private set
//    private var binding: DetailedFragmentBinding? = null
    var toastMessageMutableLiveData: MutableLiveData<String>? = null
        private set
    private var imdbID: String? = null

    val errorMutableLiveData: MutableLiveData<Error>
        get() = repository.errorMutableLiveData

    fun initViewModelFields() {
        toastMessageMutableLiveData = MutableLiveData()
        pbCurrencyMutableLiveData = repository.pbRatesMutableLiveData
        nbuCurrencyMutableLiveData = repository.nbuSingleCurrencyRatesMutableLiveData
    }

    fun onOptionsItemSelected(itemId: Int, isChecked: Boolean) {
        when (itemId) {
            R.id.delayed_message -> {
                //FIXME
            }
            R.id.offline -> saveOfflineSetToSharedPrefs(isChecked)
        }
    }

    private fun saveOfflineSetToSharedPrefs(isOfflineSet: Boolean) {
        sharedPreferences!!.edit().putBoolean(OFFLINE, isOfflineSet).apply()
    }

    fun loadRatesForPeriod(curLib: String){
        repository.loadSingleCurrencyRatesForPeriod(curLib)
    }

    companion object {

        private val TAG = RatesTabloidViewModel::class.java.simpleName
    }
}
