package com.example.myapplication3.data.models

import android.util.Log
import com.example.myapplication3.util.Converters.convertPbStringDateToLong
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlin.collections.ArrayList

class PbNetworkResponse{

    init {
        Log.d(TAG, "PbNetworkResponse created")
    }

    @SerializedName("date")
    @Expose
    val date: String? = null

    @SerializedName("bank")
    @Expose
    val bankName: String? = null

    @SerializedName("baseCurrency")
    @Expose
    val baseCurrencyCode: Int? = null

    @SerializedName("baseCurrencyLit")
    @Expose
    val baseCurrencyLit: String? = null

    @SerializedName("exchangeRate")
    @Expose
    val pbCurrencyModelList: List<PbCurrencyModel> = ArrayList<PbCurrencyModel>(19)

    fun getStoredCurrencyModelList():List<PbCurrency>{
        return pbCurrencyModelList.map { it.getStoredCurrencyModel() }.onEach{
            it.date = convertPbStringDateToLong(date)}.toList()
    }

    inner class PbCurrencyModel{

        @SerializedName("saleRateNB")
        @Expose
        val saleRateNB: Float? = null

        @SerializedName("purchaseRateNB")
        @Expose
        val purchaseRateNB: Float? = null

        @SerializedName("currency")
        @Expose
        val currency: String? = null

        @SerializedName("baseCurrency")
        @Expose
        val baseCurrency: String? = null

        @SerializedName("saleRate")
        @Expose
        val saleRate: Float? = null

        @SerializedName("purchaseRate")
        @Expose
        val purchaseRate: Float? = null

        fun getStoredCurrencyModel(): PbCurrency {
            Log.d(TAG, "getStoredCurrencyModel() created purchaseRateNB: "+purchaseRateNB)
            Log.d(TAG, "getStoredCurrencyModel() created currency: "+currency)
            return PbCurrency(saleRateNB = saleRateNB, purchaseRateNB = purchaseRateNB, currency = currency,
                    saleRate = saleRate, purchaseRate = purchaseRate)
        }
    }

    companion object {
        val TAG = PbNetworkResponse::class.java.simpleName
    }
}