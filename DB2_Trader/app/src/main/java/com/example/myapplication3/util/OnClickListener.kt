package com.example.myapplication3.util

import android.os.Bundle

interface OnClickListener {
    fun openDetailedFragment(bundle: Bundle, backStackTag: String)
}
