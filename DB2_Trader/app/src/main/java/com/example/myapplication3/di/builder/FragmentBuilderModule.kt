package com.example.myapplication3.di.builder

import com.example.myapplication3.displays.rates.graph.GraphFragment
import com.example.myapplication3.displays.rates.summary.RatesTabloidFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun contibuteGridFragment(): RatesTabloidFragment

    @ContributesAndroidInjector
    internal abstract fun contibuteDetailFragment(): GraphFragment
}
