package com.example.myapplication3.di

import com.example.myapplication3.displays.rates.ViewModelFactory
import com.example.myapplication3.displays.rates.graph.GraphViewModel
import com.example.myapplication3.displays.rates.summary.RatesTabloidViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(GraphViewModel::class)
    abstract fun bindMovieDetailsViewModel(graphViewModel: GraphViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RatesTabloidViewModel::class)
    abstract fun bindMovieRequestsViewModel(ratesTabloidViewModel: RatesTabloidViewModel): ViewModel

    @Binds
    abstract fun bindsViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

}
