package com.example.myapplication3.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = arrayOf("date", "currency"), tableName = "currencies")
class PbCurrency(
        var saleRateNB: Float?, var purchaseRateNB: Float?, currency: String?,
        var saleRate: Float? = null, var purchaseRate: Float? = null){

    var currency: String = if(currency!=null){currency}else{"AUD"}
    var date: Long = 0L

    //todo add convertor to NbuSimpleCurrency
}