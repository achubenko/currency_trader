package com.example.myapplication3.data.database
import androidx.room.Dao
import androidx.room.Query
import com.example.myapplication3.data.models.PbCurrency

@Dao
interface CurrencyRatesDao {

//    @Query("SELECT * FROM currencies")
//    fun all(): List<MovieRequest>

    @Query("SELECT * FROM currencies WHERE date LIKE :date")
    fun getAllCurrenciesForDateFromDb(date: Long): List<PbCurrency>
//
//    @Insert
//    fun insert(movieRequest: MovieRequest)
//
//    @Delete
//    fun delete(movieRequest: MovieRequest)

}
