package com.example.myapplication3.utils

import org.hamcrest.Matcher
import org.hamcrest.StringDescription

import android.util.Log
import android.view.View
import android.view.ViewGroup

import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion

import org.hamcrest.MatcherAssert.assertThat


class RecyclerViewItemSpecificityView(private val mSpecificallyId: Int, private val mMatcher: Matcher<View>) : ViewAssertion {

    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {

        val description = StringDescription()
        description.appendText("'")
        mMatcher.describeTo(description)

        val itemRoot = view as ViewGroup
        val typeIUsage = itemRoot.findViewById<View>(mSpecificallyId)

        if (noViewFoundException != null) {
            description.appendText(
                    String.format(
                            "' check could not be performed because view with id '%s' was not found.\n",
                            mSpecificallyId))
            Log.e("RecyclerViewItemSpecificityView", description.toString())
            throw noViewFoundException
        } else {
            description.appendText("' doesn't match the selected view.")
            assertThat(description.toString(), typeIUsage, mMatcher)
        }
    }
}


